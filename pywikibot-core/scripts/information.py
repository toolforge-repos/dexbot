from pywikibot import pagegenerators
import pywikibot
from collections import defaultdict
site = pywikibot.Site('fa')
cat_title = u'پرونده‌های بدون توضیح قابل خواندن برای ماشین'
gen = pagegenerators.CategorizedPageGenerator(
    pywikibot.Category(site, cat_title))
people_and_files = defaultdict(list)
for page in gen:
    print(page.title())
    author = page.oldest_revision.user
    people_and_files[author].append(page.title())
print(len(people_and_files))
for person in people_and_files:
    user_talk_page = pywikibot.Page(site, 'User talk:%s' % person)
    try:
        text = user_talk_page.get()
    except:
        continue
    if 'Dexbot]]' in text:
        continue
    one = len(people_and_files[person]) > 1
    text += u'\n== افزودن الگوی اطلاعات به تصاویرتان ==\nدرود. '
    if not one:
        text += u'یکی از پرونده‌های شما'
    else:
        text += u'تعدادی از پرونده‌های شما'
    text += ' فاقد [[الگو:اطلاعات|الگوی اطلاعات]] هستند که وجود '
    text += 'آن برای پرونده‌ها حیاتی است. فهرست آنها:\n'
    for file_name in people_and_files[person]:
        text += '*[[:%s]]\n' % file_name
    text += u'لطفا این الگو را به تصاویرتان بیفزایید. '
    text += 'اگر این تصاویر زیاد هستند از [[ویژه:ترجیحات#mw-prefsection'
    text += '-gadgets|ابزارها]] «ابزار افزودن الگوی اطلاعات به تصاویری که '
    text += 'آن را ندارند.» را فعال کرده و از آن استفاده کنید.'
    text += u' خاکسار شما، ~~~~'
    summary = u'ربات: افزودن پیامی در رابطه با تصاویری فاقد الگوی اطلاعات'
    user_talk_page.put(text, summary)
