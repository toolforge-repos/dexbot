from pywikibot import xmlreader
import re
import os

dump = xmlreader.XmlDump(
    "/public/dumps/public/fawiki/20160820/"
    "fawiki-20160820-pages-articles.xml.bz2"
)
_base_dir = '/data/project/dexbot/pywikipedia-git/'
files = ['bad_navbox_empty.txt', 'bad_navbox_wrong.txt']

for file_name in files:
    with open(os.path.join(_base_dir, file_name), 'w') as f:
        f.write('')

for entry in dump.new_parse():
    if entry.ns != '10':
        continue
    if '{{navbox' not in entry.text.lower():
        continue
    res = re.findall(r'\| *?name *?= *?(.+?)(?:\n|\||\}\})', entry.text)
    print(entry.id)
    if not res:
        with open(_base_dir + 'bad_navbox_empty.txt', 'a') as f:
            f.write('*[[%s]]\n' % entry.title)
            continue
    if res[0].replace(' ', '_') != entry.title.replace(' ', '_'):
        with open(_base_dir + 'bad_navbox_wrong.txt', 'a') as f:
            f.write('*[[%s]]\n' % entry.title)
            continue
