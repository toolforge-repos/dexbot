import pywikibot
import re
langs = [
    'en', 'sv', 'nl', 'de', 'fr', 'ru', 'it', 'es', 'vi', 'war', 'ceb',
    'pl', 'ja', 'pt', 'zh', 'uk', 'ca', 'no', 'fa', 'fi', 'id', 'ar',
    'cs', 'ko', 'ms', 'hu', 'ro', 'sr', 'tr', 'min', 'sh', 'kk', 'eo',
    'sk', 'eu', 'da', 'lt', 'bg', 'he', 'hr', 'sl', 'uz', 'hy', 'et',
    'vo', 'nn', 'gl', 'simple', 'hi', 'la', 'el', 'az', 'th', 'oc',
    'ka', 'mk', 'be', 'new', 'tt', 'pms', 'tl', 'ta', 'te', 'cy', 'lv',
    'be-tarask', 'ht', 'ur', 'ce', 'bs', 'sq', 'br', 'jv', 'mg', 'lb',
    'mr', 'is', 'ml', 'pnb', 'ba', 'af', 'my', 'zh-yue', 'bn', 'ga',
    'lmo', 'yo', 'fy', 'an', 'cv', 'tg', 'ky', 'sw', 'ne', 'io', 'gu',
    'bpy', 'sco', 'scn', 'nds', 'ku', 'ast', 'qu', 'su', 'als', 'gd',
    'kn', 'am', 'ckb', 'ia', 'nap', 'bug', 'bat-smg', 'wa', 'map-bms',
    'mn', 'arz', 'pa', 'mzn', 'si', 'zh-min-nan', 'yi', 'sah', 'fo',
    'vec', 'sa', 'bar', 'nah', 'os', 'roa-tara', 'pam', 'or', 'hsb',
    'se', 'li', 'mrj', 'mi', 'ilo', 'co', 'hif', 'bcl', 'gan', 'frr',
    'bo', 'rue', 'glk', 'mhr', 'nds-nl', 'fiu-vro', 'ps', 'tk', 'pag',
    'vls', 'gv', 'xmf', 'diq', 'km', 'kv', 'zea', 'csb', 'crh', 'hak',
    'vep', 'sc', 'ay', 'dv', 'so', 'zh-classical', 'nrm', 'rm', 'udm',
    'koi', 'kw', 'ug', 'stq', 'lad', 'wuu', 'lij', 'eml', 'fur', 'mt',
    'bh', 'as', 'cbk-zam', 'gn', 'pi', 'pcd', 'szl', 'gag', 'ksh',
    'nov', 'ang', 'ie', 'nv', 'ace', 'ext', 'frp', 'mwl', 'ln', 'sn',
    'lez', 'dsb', 'pfl', 'krc', 'haw', 'pdc', 'kab', 'xal', 'rw', 'myv',
    'to', 'arc', 'kl', 'bjn', 'kbd', 'lo', 'ha', 'pap', 'tpi', 'av',
    'lbe', 'mdf', 'jbo', 'na', 'wo', 'bxr', 'ty', 'srn', 'ig', 'nso',
    'kaa', 'kg', 'tet', 'ab', 'ltg', 'zu', 'za', 'cdo', 'tyv', 'chy',
    'tw', 'rmy', 'roa-rup', 'cu', 'tn', 'om', 'chr', 'got', 'bi', 'pih',
    'rn', 'sm', 'bm', 'ss', 'iu', 'sd', 'pnt', 'ki', 'xh', 'ts', 'ee',
    'ak', 'ti', 'fj', 'lg', 'ks', 'ff', 'sg', 'ny', 've', 'cr', 'st',
    'dz', 'ik', 'tum', 'ch',
]
for lang in langs:
    site = pywikibot.Site(lang)
    params = {
        'action': 'query',
        'list': 'search',
        'srwhat': 'text',
        'srsearch': "insource:/\n *\<nowiki\> *\<\/nowiki\>/i",
        'srnamespace': '0',
        'srinterwiki': 1,
        'srlimit': 50,
        'rawcontinue': 1,
        'srprop': 'size'
    }
    continue_res = True
    while continue_res:
        try:
            res = pywikibot.data.api.Request(site=site, **params).submit()
        except pywikibot.data.api.APIMWException:
            continue_res = False
            continue
        continue_res = res.get('query-continue')
        print(lang, res['query']['searchinfo'])
        for case in res['query']['search']:
            page = pywikibot.Page(site, case['title'])
            try:
                text = page.get()
                page.text = re.sub(r"\n *<nowiki> *</nowiki>", r'\n', text)
                if text != page.text:
                    page.save('Bot: Parsoid bug [[phab:T107675]]')
            except KeyboardInterrupt:
                break
            except:
                continue
        if continue_res:
            params['sroffset'] = continue_res['search']['sroffset']
