# -*- coding: utf-8  -*-
import pywikibot
import json
import urllib.request
import urllib.parse
import sys
import math

from collections import defaultdict, OrderedDict
import numpy as np


def mean_func(gen):
    mean = 0
    c = 0
    if not gen:
        return 0
    for case in gen:
        c += 1
        mean += case
    return mean/float(c)


def std(gen, mean=None):
    if not gen:
        return 0
    if not mean:
        mean = mean_func(gen)
    variance = 0
    c = 0
    for case in gen:
        c += 1
        variance += (case - mean)**2
    return math.sqrt(variance / float(c))


def scale(x):
    x_for_scaling = {}
    for case in x:
        for i in range(len(case)):
            x_for_scaling[i] = x_for_scaling.get(i, []) + [case[i]]

    mean_and_std = {}

    for i in x_for_scaling:
        mean = mean_func(x_for_scaling[i])
        std_var = std(x_for_scaling[i], mean)
        mean_and_std[i] = (mean, std_var)

    res = []
    for case in x:
        new_case = []
        for i in range(len(case)):
            new_case.append(
                (case[i] - mean_and_std[i][0]) / mean_and_std[i][1])
        res.append(tuple(new_case))
    return res


class User(pywikibot.page.User):
    """docstring for User"""
    def __init__(self, site, user_name):
        super(User, self).__init__(
            source=site, name='User:' + user_name)

    def articles_created(self, time=7*1440*60):
        params = {
            'action': 'query',
            'list': 'recentchanges',
            'rcstart': 'now',
            'rcnamespace': 0,
            'rcuser': self.username,
            'rcprop': 'title|comment',
            'rcshow': '!bot',
            'rclimit': 20,
            'rctype': 'new'
        }

        req = pywikibot.data.api.Request(site=self.site, **params)
        articles = []
        res = req.submit()
        for case in res['query']['recentchanges']:
            articles.append(pywikibot.Page(self.site, case['title']))
        return articles


class Bot(object):

    """docstring for Bot"""

    def __init__(self, username, site):
        self.username = username
        self.site = site
        self.user = User(site, username)
        self.data = defaultdict(dict)

    def recoms(self, article):
        url = 'http://recommend.wmflabs.org/api?s=en&t=%s&n=20&article='
        url = url % self.site.code
        try:
            req = urllib.request.urlopen(
                url + urllib.parse.quote(article.title(underscore=True)))
        except urllib.error.HTTPError:
            pass
        else:
            res = req.read().decode('utf-8')
            for case in json.loads(res)['articles']:
                yield case

    def wikidata(self, chunk):
        params = {
            'action': 'wbgetentities',
            'ids': '|'.join(chunk),
            'props': 'sitelinks'
        }
        req = pywikibot.data.api.Request(
            site=self.site.data_repository(), **params)
        res = req.submit()["entities"]
        for qid in res:
            if 'fawiki' in res[qid]['sitelinks']:
                self.data[qid]['create?'] = False
            self.data[qid]['no_iw'] = float(len(res[qid]['sitelinks']))

    @staticmethod
    def chunks(l, n):
        """Yield successive n-sized chunks from l."""
        for i in range(0, len(l), n):
            yield l[i:i+n]

    def run(self):
        ensite = pywikibot.Site('en')
        for article in self.user.articles_created():
            for iw_page in article.langlinks():
                if iw_page.site == ensite:
                    en_article = iw_page
                    break
            else:
                continue
            en_article = pywikibot.Page(ensite, en_article.title)
            for case in self.recoms(en_article):
                wd_id = self.data[case['wikidata_id']].get('no_occur', 0)
                self.data[case['wikidata_id']]['pageviews'] = \
                    float(case["pageviews"])
                self.data[case['wikidata_id']]['title'] = case["title"]
                self.data[case['wikidata_id']]['no_occur'] = float(wd_id + 1)

        for chunk in self.chunks(list(self.data.keys()), 50):
            self.wikidata(chunk)
        c = 0
        array = []
        key_dict = {}
        for qid in self.data:
            key_dict[c] = qid
            case = self.data[qid]
            array.append([case['no_occur'], case['no_iw'], case['pageviews']])
            c += 1
        X = np.array(array)
        X_scaled = scale(X)
        final_res = {}
        for i in range(len(array)):
            final_res[key_dict[i]] = (3 * X_scaled[i][0]) +\
                (2 * X_scaled[i][1]) + (X_scaled[i][2])
        final_res = OrderedDict(
            sorted(final_res.items(), key=lambda t: t[1], reverse=True))
        self.res_to_print = []
        for case in final_res:
            if self.data[case].get('create?', True):
                title = self.data[case]['title'].replace('_', ' ')
                self.res_to_print.append(title)


def main(args):
    site = pywikibot.Site('fa')
    bot = Bot(args[0], site)
    bot.run()
    print(bot.res_to_print[:20])

if __name__ == "__main__":
    main(sys.argv[1:])
