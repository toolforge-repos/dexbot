import pywikibot
import codecs
site = pywikibot.Site('commons', fam='commons')
with codecs.open('/data/project/dexbot/res_final.txt', 'r', 'utf-8') as f:
    files = f.read().split('\n')

selfs = []
PDs = []
PDusers = []
very_final = []
c = 0
for title in files:
    print(c)
    title = title.strip()
    c_file = pywikibot.ImagePage(site, 'File:' + title)
    categories = [i.title() for i in c_file.categories()]
    final = True
    if 'Category:Self-published work' in categories:
        selfs.append(title)
        final = False
    if 'Category:PD' in '|'.join(categories):
        final = False
        PDs.append(title)
        if 'Category:PD-user' in categories:
            PDusers.append(title)
    if final:
        very_final.append(title)
    c += 1
print(len(selfs), len(PDs), len(PDusers), len(very_final))
with codecs.open('/data/project/dexbot/res_final1.txt', 'w', 'utf-8') as f:
    f.write(u'\n'.join(selfs))
with codecs.open('/data/project/dexbot/res_final2.txt', 'w', 'utf-8') as f:
    f.write(u'\n'.join(PDs))
with codecs.open('/data/project/dexbot/res_final3.txt', 'w', 'utf-8') as f:
    f.write(u'\n'.join(PDusers))
with codecs.open('/data/project/dexbot/res_final4.txt', 'w', 'utf-8') as f:
    f.write(u'\n'.join(very_final))
