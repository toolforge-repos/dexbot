import re
import pywikibot
linkcache = {}
ensite = pywikibot.Site('en')
template = """<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:CardiologyApp.jpg|125px|link=Book:Cardiology]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Cardiology]]'''<br/>[[Abdominal aortic aneurysm]] • [[Aortic stenosis]] • [[Atrial fibrillation]] • [[Cardiac arrhythmia]] • [[Cardiovascular disease]] • [[Coronary artery disease]] • [[Heart failure]] • [[Myocardial infarction]] • [[Peripheral artery disease]] • [[Pulmonary embolism]] • [[Rheumatic fever]] • [[Syncope]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#d8d8ff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:PedsApp.jpg|125px|link=Book:Pediatrics]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Pediatrics|Children's health]]'''<br/>[[Asperger syndrome]] • [[Circumcision]] • [[Cleft lip and palate]] • [[Congenital heart defect]] • [[Down syndrome]] • [[Epilepsy]] • [[Female genital mutilation]] • [[Fetal alcohol spectrum disorder]] • [[Klinefelter syndrome]] • [[Sickle-cell disease]] • [[Spina bifida]] • [[Sudden infant death syndrome]] • [[Turner syndrome]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:DermApp.jpg|125px|link=Book:Dermatology]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Dermatology]]'''<br/>[[Abscess]] • [[Acne vulgaris]] • [[Allergy]] • [[Angular cheilitis]] • [[Atopic dermatitis]] • [[Candidiasis]] • [[Cellulitis]] • [[Chickenpox]] • [[Dermatitis]] • [[Hair loss]] • [[Head lice infestation]] • [[Herpes simplex]] • [[Herpes zoster]] • [[Measles]] • [[Psoriasis]] • [[Scabies]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#d8d8ff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:ENTApp.jpg|125px|link=Book:Ears nose throat]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Ears nose throat]]'''<br/>[[Benign paroxysmal positional vertigo]] • [[Hearing loss]] • [[Mandibular fracture]] • [[Nasal polyp]] • [[Nose bleed]] • [[Otitis externa]] • [[Otitis media]] • [[Pharyngitis]] • [[Strep throat]] • [[Tinnitus]] • [[Vertigo]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:EndoApp.png|125px|link=Book:Endocrinology]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Endocrinology]]'''<br/>[[Addison's disease]] • [[Cushing's syndrome]] • [[Delirium tremens]] • [[Diabetes]] • [[DM type 1]] • [[DM type 2]] • [[Gestational diabetes]] • [[Graves' disease]] • [[Hypernatremia]] • [[Hyperthyroidism]] • [[Hypoglycemia]] • [[Hyponatremia]] • [[Hypothyroidism]] • [[Obesity]] • [[Primary hyperaldosteronism]] • [[Vitamin B12 deficiency]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#d8d8ff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:GenSurgApp.jpg|125px|link=Book:General surgery]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[General surgery]]'''<br/>[[Appendicitis]] • [[Bowel obstruction]] • [[Celiac disease]] • [[Cholecystitis]] • [[Crohn's disease]] • [[Diarrhea]] • [[Gastritis]] • [[Gastrointestinal bleeding]] • [[Gastrointestinal perforation]] • [[Hemorrhoid]] • [[Hernia]] • [[Irritable bowel syndrome]] • [[Pancreatitis]] • [[Peptic ulcer disease]] • [[Pernicious anemia]] • [[Ulcerative colitis]] • [[Volvulus]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:IDApp.JPG|125px|link=Book:Infectious disease]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Infectious disease]]'''<br/>[[African trypanosomiasis]] • [[Ascariasis]] • [[Buruli ulcer]] • [[Cellulitis]] • [[Chagas disease]] • [[Common cold]] • [[Cysticercosis]] • [[Drancunculiasis]] • [[Ebola virus disease]] • [[Hepatitis A]] • [[Hepatitis B]] • [[Hepatitis C]] • [[HIV/AIDS]] • [[Leprosy]] • [[Lyme disease]] • [[Malaria]] • [[Meningitis]] • [[Rabies]] • [[Syphilus]] • [[Tuberculosis]] • [[Yellow fever]] • [[Zika fever]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#d8d8ff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:Rx symbol border.svg|125px|link=Book:Medications]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Medications]]'''<br/>[[Birth control]] • [[Carbamazapine]] • [[Cephalexin]] • [[Cholera vaccine]] • [[Cocaine]] • [[Dapsone]] • [[Diazepam]] • [[Hydrochlorothiazide|HCTZ]] • [[Ibuprofen]] • [[Influenza vaccine]] • [[Ipratropium bromide]] • [[Ketamine]] • [[Levofloxacin]] • [[Measles vaccine]] • [[Metoprolol]] • [[Mifepristone]] • [[Morphine]] • [[Nystatin]] • [[Paracetamol]] (acetaminophen) • [[Propofol]] • [[Salbutamol]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:CancerApp.jpg|125px|link=Book:Cancer]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Cancer|Oncology]]'''<br/>[[Brain tumor]] • [[Breast cancer]] • [[Cancer]] • [[Cervical cancer]] • [[Colon cancer]] • [[Endometrial cancer]] • [[Esophageal cancer]] • [[Glioblastoma multiforme]] • [[Leukemia]] • [[Lung cancer]] • [[Lymphoma]] • [[Melanoma]] • [[Mesothelioma]] • [[Ovarian cancer]] • [[Pancreatic cancer]] • [[Prostate cancer]] • [[Skin cancer]] • [[Stomach cancer]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#d8d8ff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:OpthoApp.png|125px|link=Book:Ophthalmology]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Ophthalmology]]'''<br/>[[Amblyopia]] • [[Cataracts]] • [[Color blind]] • [[Conjunctivitis]] • [[Conjunctivitis]] • [[Detached retina]] • [[Dry eye]] • [[Glaucoma]] • [[Macular degeneration]] • [[Refractive error]] • [[Trachoma]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:PsycApp.JPG|125px|link=Book:Psychiatry]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Psychiatry]]'''<br/>[[ADHD]] • [[Alcoholism]] • [[Anorexia nervosa]] • [[Anxiety disorder|Anxiety]] • [[Autism]] • [[Bipolar disorder]] • [[Borderline personality disorder]] • [[Bulimia nervosa]] • [[Eating disorder]] • [[Major depressive disorder|Depression]] • [[Obsessive-compulsive disorder]] • [[Phobia]] • [[Post traumatic stress disorder]] • [[Schizophrenia]] • [[Suicide]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#d8d8ff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:RheumApp.JPG|125px|link=Book:Rheumatology]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Rheumatology]]'''<br/>[[Carpal tunnel syndrome]] • [[Fibromyalgia]] • [[Gout]] • [[Low back pain]] • [[Osteoarthritis]] • [[Osteoporosis]] • [[Plantar fasciitis]] • [[Psoriasis]] • [[Rheumatoid arthritis]] • [[Sarcoidosis]] • [[Sciatica]]</td>
</tr></table>
<table class="multicol" style="border-spacing:0;background:#ebebff;" role="presentation">
<tr style="vertical-align:top;">
<td style="width:125px;">
[[Image:OBGYNApp.jpg|125px|link=Book:Women's health]]</td>
<td style="width:0.5em;"></td>
<td style="width:auto;">
<div style="height: 0.5em;"/>'''[[Women's health]]'''<br/>[[Abortion]] • [[Breastfeeding]] • [[Childbirth]] • [[Dysmenorrhea]] • [[Eclampsia]] • [[Ectopic pregnancy]] • [[Endometriosis]] • [[Hyperemesis gravidarum]] • [[Menopause]] • [[Menstruation]] • [[Morning sickness]] • [[Obstructed labor]] • [[Ovarian cyst]] • [[Polycystic ovarian syndrome]] • [[Pre eclampsia]] • [[Pregnancy]] • [[Premenstrual syndrome]] • [[Preterm birth]] • [[Trichomoniasis]] • [[Uterine fibroid]]</td>
</tr></table>"""


def linker(a):
    if not a:
        return a
    if u"[[" in a:
        return a
    if "(" in a:
        return u"[[" + a + u"|]]"
    return u"[[%s]]" % a


def translator(a, lang):
    b = a
    rerf = re.compile("\[\[(.+?)(?:\||\]\])")
    fasite = pywikibot.Site(lang)
    for name in rerf.findall(a):
        if linkcache.get((name, lang), None):
            b = re.sub(u"\[\[%s(?:\|.+?)?\]\]" %
                       re.escape(name), linkcache[(name, lang)], b)
        else:
            try:
                pagename = pywikibot.Page(ensite, name)
            except:
                continue
            if pagename.isRedirectPage():
                pagename = pagename.getRedirectTarget()
            try:
                iwpages = pagename.langlinks()
            except:
                continue
            for iwpage in iwpages:
                if iwpage.site == fasite:
                    linkcache[(name, lang)] = linker(iwpage.canonical_title())
                    b = re.sub(u"\[\[%s(?:\|.+?)?\]\]" %
                               re.escape(name), linker(iwpage.canonical_title()), b)
    #b = b.replace("""<div style="height: 0.5em;"/>'''[[""", """<div style="height: 0.5em;"/>'''[[Book:""")
    #b = b.replace("""]]'''<br/>[[""","""|]]'''<br/>[[""")
    return b


langs = [
            'th', 'ka',
            'ce', 'oc', 'be', 'mk', 'mg', 'new', 'ur', 'tt', 'ta', 'pms', 'cy',
            'tl', 'lv', 'bs', 'te', 'be-tarask', 'br', 'ht', 'sq', 'jv', 'lb',
            'mr', 'is', 'ml', 'zh-yue', 'bn', 'af', 'ba', 'ga', 'pnb', 'cv',
            'fy', 'lmo', 'tg', 'sco', 'my', 'yo', 'an', 'ky', 'sw', 'io', 'ne',
            'gu', 'scn', 'bpy', 'nds', 'ku', 'ast', 'qu', 'als', 'su', 'pa',
            'kn', 'ckb', 'ia', 'mn', 'nap', 'bug', 'arz', 'bat-smg', 'wa',
            'zh-min-nan', 'am', 'map-bms', 'gd', 'yi', 'mzn', 'si', 'fo', 'bar',
            'vec', 'nah', 'sah', 'os', 'sa', 'roa-tara', 'li', 'hsb', 'or',
            'pam', 'mrj', 'mhr', 'se', 'mi', 'ilo', 'hif', 'bcl', 'gan', 'rue',
            'ps', 'glk', 'nds-nl', 'bo', 'vls', 'diq', 'fiu-vro', 'bh', 'xmf',
            'tk', 'gv', 'sc', 'co', 'csb', 'hak', 'km', 'kv', 'vep', 'zea',
            'crh', 'zh-classical', 'frr', 'eml', 'ay', 'stq', 'udm', 'wuu',
            'nrm', 'kw', 'rm', 'szl', 'so', 'koi', 'as', 'lad', 'fur', 'mt',
            'dv', 'gn', 'dsb', 'ie', 'pcd', 'sd', 'lij', 'cbk-zam', 'cdo',
            'ksh', 'ext', 'mwl', 'gag', 'ang', 'ug', 'ace', 'pi', 'pag', 'nv',
            'lez', 'frp', 'sn', 'kab', 'ln', 'myv', 'pfl', 'xal', 'krc', 'haw',
            'rw', 'pdc', 'kaa', 'to', 'kl', 'arc', 'nov', 'kbd', 'av', 'bxr',
            'lo', 'bjn', 'ha', 'tet', 'tpi', 'na', 'pap', 'lbe', 'jbo', 'ty',
            'mdf', 'roa-rup', 'wo', 'tyv', 'ig', 'srn', 'nso', 'kg', 'ab',
            'ltg', 'zu', 'om', 'za', 'chy', 'cu', 'rmy', 'tw', 'tn', 'chr',
            'mai', 'pih', 'got', 'xh', 'bi', 'sm', 'ss', 'rn', 'ki', 'pnt',
            'bm', 'iu', 'ee', 'lg', 'ts', 'fj', 'ak', 'ik', 'st', 'sg', 'ff',
            'dz', 'ny', 'ch', 'ti', 've', 'ks', 'tum', 'cr', 'gom', 'lrc',
            'azb',
]

for lang in langs:
    site = pywikibot.Site(lang)
    page = pywikibot.Page(site, 'User:Doc James/Open Textbook of Medicine')
    try:
        page.get()
    except pywikibot.NoPage:
        try:
            page.put(translator(template, lang), 'Bot: Making user subpage upon request')
        except pywikibot.NoPage:
            continue
    else:
        continue

