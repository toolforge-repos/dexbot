import codecs
import pywikibot
import re

with codecs.open('uni.txt', 'r', 'utf-8') as f:
    articles = re.findall(r'\[\[(.+?)\]\]', f.read())
site = pywikibot.Site('en')
fasite = pywikibot.Site('fa')

_cache = {}
res = u""


def get_fa(item):
    if item not in _cache:
        try:
            _cache[item] = item.getSitelink(fasite)
        except:
            _cache[item] = ''
    return _cache[item]


for article in articles:
    try:
        item = pywikibot.ItemPage.fromPage(pywikibot.Page(site, article))
    except pywikibot.NoPage:
        continue
    countries = []
    if item.claims.get('P17'):
        for claim in item.claims['P17']:
            countries.append(get_fa(claim.target))
    res += u"*[[%s]]: %s\n" % (article, u''.join(countries))
    with codecs.open('uni_res.txt', 'w', 'utf-8') as f:
        f.write(res)
