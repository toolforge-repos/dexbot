import codecs
import pywikibot
import mwparserfromhell
with codecs.open('hospital.txt', 'r', 'utf-8') as f:
    titles = f.read().split('\n')
site = pywikibot.Site('en')
res = u""
line = u""
for title in titles:
    #print(title)
    if not title.strip():
        continue
    res += u'\n*' + title.strip()
    line = '*' + title.strip()
    page = pywikibot.Page(site, title.strip())
    try:
        text = page.get()
    except:
        continue

    wikicode = mwparserfromhell.parse(text.split('\n==')[0])
    templates = wikicode.filter_templates()
    for template in templates:
        #print(unicode(template.name).lower())
        if unicode(template.name).lower().strip() == 'infobox hospital':
            #print(template.name)
            if template.has('beds'):
                res += u'\t' + unicode(template.get('beds').value)
                line = line + u'\t' + unicode(template.get('beds').value)
    print(line)
    #break
with codecs.open('res_hospital.txt', 'w', 'utf-8') as f:
    f.write(res)
