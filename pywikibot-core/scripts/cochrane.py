import pywikibot
import re
import urllib2

from pywikibot import pagegenerators

site = pywikibot.Site('en')
generator = pagegenerators.SearchPageGenerator(
    'insource:/\| *journal *= *.+Cochrane/',
    site=site,
    namespaces=[0]
)
gen = pagegenerators.PreloadingGenerator(generator)


def update_report(article, old_pmid, new_pmid, ):
    report = pywikibot.Page(
        site,
        'Wikipedia:WikiProject_Medicine/Cochrane_update/June 2016')
    report_text = report.get()
    rep = (u'\n*Article [[%s]] ([{{fullurl:%s|action=edit}} edit]) '
           u'old review [https://www.ncbi.nlm.nih.gov/pubmed/%s '
           u'PMID:%s] new review [https://www.ncbi.nlm.nih.gov/pubmed/'
           u'%s PMID:%s]' % (
            article.title(), article.title(), old_pmid, old_pmid,
            new_pmid, new_pmid))
    if rep in report_text:
        return
    report.text = report_text + rep + u' - ~~~~~'
    report.save('Bot: Update report')


for page in gen:
    try:
        text = page.get()
    except:
        continue
    if '<!-- No update needed -->' in text:
        continue
    pmids = re.findall(r'\|\s*?pmid\s*?=\s*?(\d+?)\s*?\|', text)
    print(len(pmids))
    for pmid in pmids:
        try:
            res = urllib2.urlopen(
                'https://www.ncbi.nlm.nih.gov/pubmed/%s' % pmid)
            res = res.read().decode('utf-8')
        except:
            continue
        if 'WITHDRAWN' in res:
            continue
        pattern = (r'<h3>Update in</h3><ul><li class="comments">'
                   r'<a href="/pubmed/\d+?"')
        if re.search(pattern, res):
            pm = re.findall(r'<h3>Update in</h3><ul><li class="comments">'
                            r'<a href="/pubmed/(\d+?)"', res)[0]
            up = (u'{{Update inline|reason=Updated version '
                  u'https://www.ncbi.nlm.nih.gov/pubmed/' + pm)
            if up not in text:
                text = re.sub(r'(\|\s*?pmid\s*?=\s*?%s\s*?'
                              r'(?:\||\}\}).*?< *?/ *?ref *?>)'
                              % pmid, r'\1%s}}' % up, text, re.DOTALL)
            update_report(page, pmid, pm)
    if text != page.text:
        page.text = text
        page.save(u'Bot: Adding "update inline" template')
