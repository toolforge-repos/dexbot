import pywikibot
#import cache
import urllib.request
import time
import json
_cache = {}
site = pywikibot.Site('fa')
url = "https://ores.wikimedia.org/scores/fawiki/?models=reverted|goodfaith&revids="
summary = u'ربات: [[وپ:رمبو|واگردانی خرابکاری محتمل]] {0:.3},{1},{2}'

version = '0.2.1'

def report(diff_id, d_score, g_score):
    file_path = '/data/project/dexbot/public_html/tools/good_faith_editors.txt'
    with open(file_path, 'r') as f:
        the_list = f.read()
    if diff_id + ',' in the_list:
        return
    with open(file_path, 'w') as f:
        f.write(','.join([str(diff_id), str(d_score), str(g_score)]) + '\n' + the_list)

def recent_changes_gen(site):
    params = {
        'action': 'query',
        'list': 'recentchanges',
        'rcshow': '!bot|!patrolled',
        'rclimit': 100,
        'rctype': 'edit',
        'rctoponly': '1',
        'rcprop': 'ids',
        'rcnamespace': '0|1|2|5|6|7|8|9|10|11|12|13|14|15|100|101|102|103|118|119|2300|2301|2302|2303|446|447|2600|828|829',
    }
    res = pywikibot.data.api.Request(site=site, **params).submit()
    for case in res['query']['recentchanges']:
        yield str(case['revid'])


class Ghaher69Bot(pywikibot.Bot):
    """docstring for Ghaher69Bot"""
    def __init__(self, gen, site):
        super(Ghaher69Bot, self).__init__()
        self.gen = gen
        self.site = site
    def run(self):
        self.site.login()
        rees = list(self.cache_filter())
        self.url = url + u'|'.join(rees)
        if not rees:
             print('not working')
             return
        #print(self.url, list(self.cache_filter()))
        res = json.loads(urllib.request.urlopen(self.url).read().decode('utf-8'))
        #try:
        #    res = json.loads(urllib.request.urlopen(self.url).read().decode('utf-8'))
        #except:
        #    print('Re-running')
        #    time.sleep(20)
        #    self.run()
        #print(res)
        for revid in res:
            _cache[revid] = res[revid]['reverted']
            try:
                d_score = res[revid]['reverted']['probability']['true']
                g_score = res[revid]['goodfaith']['probability']['true']
            except:
                continue
            #print(revid, d_score, g_score)
            if d_score > 0.80:
                if g_score < 0.5:
                    try:
                        self.fuck_it(revid, d_score)
                    except pywikibot.data.api.APIError:
                        continue
            #if d_score > 0.90 and g_score > 0.5:
            #    report(revid, d_score, g_score)
    def cache_filter(self):
        for rev in self.gen:
            if not rev in _cache:
                yield rev

    def fuck_it(self, revid, probability):
        params = {
            'action': 'query',
            'prop': 'revisions',
            'meta': 'tokens',
            'rvprop':'user',
            'type': 'rollback',
            'revids': revid,
        }
        res = pywikibot.data.api.Request(site=self.site, **params).submit()
        token = res['query']['tokens']['rollbacktoken']
        page_id = list(res['query']['pages'].keys())[0]
        user = res['query']['pages'][page_id]['revisions'][0]['user']
        params = {
            'action': 'rollback',
            'pageid': page_id,
            'user': user,
            'summary': summary.format(probability, user, version),
            'token': token
        }
        pywikibot.data.api.Request(site=self.site, **params).submit()


bot = Ghaher69Bot(recent_changes_gen(site), site)
while True:
    bot.run()
    time.sleep(360)


