import pywikibot
import re

site = pywikibot.Site('fa')
months = ['ژانویه', 'فوریه', 'مارس', 'آوریل', 'مه', 'ژوئن', 'ژوئیه', 'اوت', 'سپتامبر', 'اکتبر', 'نوامبر', 'دسامبر']
days = ['۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹', '۱۰', '۱۱', '۱۲', '۱۳', '۱۴', '۱۵', '۱۶', '۱۷', '۱۸', '۱۹', '۲۰', '۲۱', '۲۲', '۲۳', '۲۴', '۲۵', '۲۶', '۲۷', '۲۸', '۲۹', '۳۰', '۳۱']
rep_page = pywikibot.Page(site, 'User:Ladsgroup/گزارشی بر یادبودهای برگزیده')


def report(article, note, yad_page, title):
    try:
        text = rep_page.get()
    except:
        text = ''
    yad_page2 = yad_page.split('/')[1]
    text += "\n*[[%s|%s]] ([[%s]]): %s" % (yad_page, yad_page2, title, note)
    rep_page.put(text, 'ربات: افزودن گزارشی از اشتباهات احتمالی یادبودهای برگزیده')

def fa2en(i):
    fachars=u"۰۱۲۳۴۵۶۷۸۹"
    try:
        b=str(i)
    except:
        b=i
    for i in range(0,10):
        b=b.replace(fachars[i],str(i))
    return b


def check_articles(articles, rtype, yad_page):
    yad_day = yad_page.split('ویکی‌پدیا:یادبودهای برگزیده/')[1].split(' ')[0]
    yad_month = yad_page.split('ویکی‌پدیا:یادبودهای برگزیده/')[1].split(' ')[1]
    yad_month_number = months.index(yad_month) + 1
    yad_day = int(fa2en(yad_day))
    for article in articles:
        try:
            page = pywikibot.Page(site, article)
        except:
            report(article, 'نام مشکل جدی دارد.', yad_page, article)
            continue
        if not page.exists():
            report(article, 'مقاله وجود ندارد.', yad_page, article)
            continue
        if page.isRedirectPage():
            page = page.getRedirectTarget()
        try:
            item = pywikibot.ItemPage.fromPage(page)
        except:
            report(article, 'آیتم ویکی‌داده وجود ندارد.', yad_page, article)
            continue
        p_number = 'P569' if rtype == 'b' else 'P570'
        if p_number not in item.claims:
            continue
        date = item.claims[p_number][0].getTarget()
        if not date:
            continue
        month = date.month
        day = date.day
        if day != yad_day or month != yad_month_number:
            print(day, yad_day, month, yad_month_number)
            report(article, "'''تاریخ‌ها با هم همخوانی ندارند'''", yad_page, article)


for month in months:
    for day in days:
        page = pywikibot.Page(site, 'ویکی‌پدیا:یادبودهای برگزیده/%s %s' % (day, month))
        try:
            text = page.get()
        except:
            continue
        births = re.findall(r"\* *?\[\[.+?\]\] *?\- *?زادروز *?(?:''')? *?\[\[(.+?)\]\]", text)
        deaths = re.findall(r"\* *?\[\[.+?\]\] *?\- *?درگذشت *?(?:''')? *?\[\[(.+?)\]\]", text)
        check_articles(births, 'b', page.title())
        check_articles(deaths, 'd', page.title())

