# -*- coding: utf-8  -*-
import pywikibot,codecs,time,re, pagegenerators, query, json
#'en','fa','af','sq','az','eu','bs','ca','hr','cs','da','nl','eo','et','fi','fr','gl','de','hu','is','id','it','ku','li','lt','nn',
langs=['no','pl','pt','ro','sk','sl','es','sv','tr','vi','cy','ar','hy','bg','zh','ka','el','he','ja','ko','ml','ru','sa','sr','ta','te','th','uk']
#bg,cy,el,he, 'hy','ja',
langs=['af','sq','az','eu','bs','ca','hr','cs','da','nl','en','eo','et','fi','fr','gl','de','hu','is','id','it','ku','li','lt','nn','no','pl','pt','ro','sk','sl','es','sv','tr','vi','cy','ar','hy','bg','zh','ka','el','he','ja','ko','ml','fa','ru','sa','sr','ta','te','th','uk']
exceptlangs=['ar']
langs = ['pl']
def datapedia(enname):
    page=pywikibot.Page(sitepedia,enname)
    try:
        data = pywikibot.ItemPage.fromPage(page)
    except:
        return ""
    else:
        return data.title()
def getdata(text,title):
    rerwiki=re.compile("\{\{(msg:|[Tt]emplate:|)([Ww]ikipedia.*?)\}\}")
    wikiqu=rerwiki.findall(text)
    if wikiqu:
        if "|" in wikiqu:
            name=wikiqu.split("|")[1]
        else:
            name=title
    else:
        name=title
    dataqu=datapedia(name)
    return dataqu
def report_not_okay(page):
    repo=pywikibot.Page(datasite,"User:Ladsgroup/Wikiquote integration/%s" % page.site().lang)
    try:
        text=repo.get()
    except:
        text=""
    if page.title()+u"]]" in text:
        return
    repo.put(text+u"\n*[[:q:%s:%s]]" % (page.site.lang,page.title()),"Bot: Reporting interwiki conflict")
for fucklang in langs:
    if fucklang in exceptlangs:
        continue
    site=pywikibot.Site(fucklang,fam='wikiquote')
    gen=pagegenerators.AllpagesPageGenerator("!", includeredirects=False, site=site)
    pregen=pagegenerators.PreloadingGenerator(gen)
    sitepedia=pywikibot.Site(fucklang,fam='wikipedia')
    datasite=pywikibot.Site('wikidata',fam='wikidata')
    for page in pregen:
        name=page.title()
        name=name.replace(u"\n",u"")
        if "/" in name:
            continue
        pywikibot.output(u"Proccessing "+name)
        page=pywikibot.Page(site,name)
        try:
               text=page.get(throttle=False)
        except:
               continue
        realdata=''
        if "Year page placeholder" in text:
            continue
        realdata=getdata(text, page.title())
        try:
            items=pywikibot.ItemPage.fromPage(page).get()
        except:
            pass
        else:
            print "It has been created already"
            continue
        DOIT=True
        DOITT=True
        dic={}
        try:
            fuckinterwiki=page.interwiki()
        except:
            continue
        for iwpage in fuckinterwiki:
            try:
                iwpage=pywikibot.Page(pywikibot.Site(iwpage.site.code,fam="wikiquote"),iwpage.title())
            except:
                continue
            try:
                kooft=text.split(iwpage.title())[1].split("]]")[0]
            except:
                DOITT=False
                continue
            if u"#" in kooft:
                DOITT=False
                continue
            try:
                iwtext=iwpage.get(throttle=False)
            except:
                DOITT=False
            else:
                try:
                    iwdata=pywikibot.DataPage(iwpage)
                except:
                    DOITT=False
                    continue
                try:
                    iwitems=iwdata.get(throttle=False)
                except pywikibot.NoPage:
                    dic[str(iwpage.site()).replace(u"wikiquote:",u"")]=iwpage.title()
                else:
                    DOIT=False
                    dic[str(iwpage.site()).replace(u"wikiquote:",u"")]=iwpage.title()
        if not DOIT and DOITT:
            dic2=list(set(dic.keys()))
            if iwitems['links'].get(fucklang+'wikiquote',{}):
                report_not_okay(page)
                continue
            if len(dic2)==1:
                iwdata.setSitelink(page, summary="Bot: Importing article from Wikiquote")
                continue
        dic[str(page.site()).replace(u"wikiquote:",u"")]=page.title()
        values={'sitelinks':{}}
        for lang in dic:
            print lang
            lang1=lang.replace("-","_")
            values['sitelinks'][lang1+'wikiquote']={'site':lang1+'wikiquote','title':dic[lang]}
        try:
            items=data.get(throttle=False)
        except pywikibot.NoPage:
            #checking for enlgish wikipedia:
            wikipediadata=realdata
            if wikipediadata:
                params = {
                    'summary': u"Bot: Adding label and sitelink from Wikiquote",
                    'format': 'jsonfm',
                    'action': 'wbeditentity',
                    'token':datasite.getToken(),
                    'bot':1,
                    'data': json.dumps(values),}
                params['id']=wikipediadata
                try:
                        query.GetData(params, datasite)
                except:
                        pass
            else:
                print "The item doesn't exist. Creating..."
                #data.createitem("Bot: Importing article from English Wikisource",value=values)
        else:
            print "It has been created already. Skipping..."
