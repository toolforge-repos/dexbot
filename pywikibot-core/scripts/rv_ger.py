import pywikibot
gen = pywikibot.Site('wikidata', fam='wikidata').usercontribs(user='GerardM', namespaces=0, start=u'2015-08-15T00:00:00Z', end=u'2015-08-16T00:00:00Z', reverse=True)
repo = pywikibot.Site('en').data_repository()
for case in gen:
    if '[[Property:P27]]: [[Q31]]' not in case['comment']:
        continue
    item = pywikibot.ItemPage(repo, case['title'])
    item.get()
    for claim in item.claims.get('P27', []):
        if claim.target.getID() == 'Q31':
            item.removeClaims(claim, summary='Revert per user request')
    #break

