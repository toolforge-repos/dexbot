import pywikibot
import re

site = pywikibot.Site('fa')
rep = pywikibot.Page(site, 'کاربر:Ladsgroup/Bad navbox').get()
for title in re.findall(r'\[\[(.+?)\]\]', rep):
    if '/' in title or '\\' in title:
        continue
    page = pywikibot.Page(site, title)
    try:
        text = page.get()
    except:
        continue
    if '{{navbox' not in text.lower():
        continue
    res = re.findall(r'\| *?name *?\= *?(.+?)(?:\n|\||\}\})', text)
    if not res:
        continue
    old_text = text
    if 'الگو:' + res[0].strip().replace(' ', '_') != title.replace(' ', '_'):
        if '{' in res[0] or '}' in res[0]:
            continue
        text = re.sub(r'(\| *?name *?\= *?)(.+?)(\n|\||\}\})',
                      r'\1%s\3' % title[5:], text)
    if old_text != text:
        page.put(text, 'ربات: تصحیح لینک ناوبری')
