import pywikibot
import MySQLdb as mysqldb
import json
import codecs

query = """
select distinct
    page_title,
    ct_params,
    rev_comment
from
    change_tag,
    revision,
    user,
    page,
    page_props
where
    ct_tag = 'contenttranslation' and
    page_namespace = 0 and
    rev_id = ct_rev_id and
    rev_page = page_id and
    rev_user = user_id and
    pp_page = page_id
GROUP BY page_title
HAVING
    SUM((pp_propname = 'wikibase_item')) = 0
ORDER BY null;"""
res = ""
langs = pywikibot.Site('en').family.languages_by_size
for lang in langs:
    site = pywikibot.Site(lang)
    try:
        conn = mysqldb.connect(
            lang.replace('-', '_') + "wiki.labsdb",
            db=site.dbName().replace('-', '_') + '_p',
            read_default_file="~/replica.my.cnf"
        )
    except:
        continue
    cursor = conn.cursor()

    query = query.encode(site.encoding())
    cursor.execute(query)
    print(lang)
    for case in cursor.fetchall():
        title = case[0].decode('utf-8')
        params = json.loads(case[1].decode('utf-8'))
        target = case[2].split(']]')[0].split('|')[-1].decode('utf-8')
        res += u'{title}--{from_l}--{to}--{target}\n'.format(
            title=title,
            from_l=params['from'],
            to=params['to'],
            target=target
        )
    path = '/data/project/dexbot/pywikibot-core/amire80.txt'
    with codecs.open(path, 'w', 'utf-8') as f:
        f.write(res)
