import pywikibot
from pywikibot import pagegenerators
site = pywikibot.Site('en', 'wikipedia')
repo = site.data_repository()
item = pywikibot.ItemPage(repo, 'Q1841')
item.get()
tar = pywikibot.ItemPage(repo, 'Q9592')
gen=pagegenerators.ReferringPageGenerator(item)
pregen=pagegenerators.PreloadingGenerator(gen)
for page in pregen:
    if page.namespace() == 0:
        n_item = pywikibot.ItemPage(repo, page.title())
        n_item.get()
        #print n_item.claims.keys()
        imp = n_item.claims.get('P140', [])
        for claim in imp:
            if claim.target_equals('Q1841'):
                claim.changeTarget(tar, summary='Based on [[Special:PermaLink/216202625#Roman_Catholism]]')
            break


