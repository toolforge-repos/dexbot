# -*- coding: utf-8  -*-
#!/usr/bin/python
#
# Amir Sarabadani, 2011 (developed)
import sys
import re
import urllib2
import httplib
import socket
import codecs
import ftplib
import wikipedia
import catlib
import pagegenerators
import query
import subprocess, tempfile, os,time, json
from fixes import fixes

site=wikipedia.getSite('wikidata', fam='wikidata')
Prpertynumber=int(107)
wikisite=wikipedia.getSite('en')
f=codecs.open("/data/project/dexbot/pywikipedia-git/ggg.txt","r","utf-8")
aaa=f.read()
g=codecs.open("/data/project/dexbot/pywikipedia-git/bilabel.txt","r","utf-8")
f.close()
aaa=aaa+u"\n"+g.read()
g.close()
capitalls = u"АБВГҐДЂЃЕЀЁЄЖЗЅИЍІЇЙЈКЛЉМНЊОПРСТЋЌУЎФХЦЧЏШЩЪЫЬЭЮЯӐӒӘӚӔҒҔӺӶԀԂꚈӖӁҖӜԄҘӞԐӠԆӢҊӤҚӃҠҞҜԞԚӅԒԠԈԔӍӉҢӇҤԢԊӦӨӪҨԤҦҎԖҪԌҬԎӮӰӲҮҰҲӼӾҺԦҴҶӴӋҸꚆҼҾӸҌӬԘԜӀ"
rer=re.compile("\[\[Q(.+?)\]\]")
authors = {
    "it": 102,
    "be": 102,
    "la": 102,
    "bn": 100,
    "ca": 106,
    'en': 102,
    "vec": 100,
    "bg": 100,
    "ro": 102,
    "sv": 106,
    "te": 102,
    "fr": 102,
    "da": 102,
    "he": 108,
    "et": 106,
    "vi": 102,
    "ml": 100,
    "hu": 100,
    "fa": 102,
    "is": 102,
    "hr": 100,
    "nl": 102,
    "kn": 102,
    "no": 102,
    "uk": 102,
    "id": 100,
    "pl": 104,
    "ko": 100,
    "hy": 100,
    "pt": 102,
    "mr": 102,
    "br": 104,
    "tr": 100,
    "ar": 102,
    "cs": 100}
authors2 = {}
for lang in authors:
    source_site = wikipedia.getSite(lang, fam='wikisource')
    try:
        authors2[lang] = source_site.namespace(authors[lang])
    except:
        print(lang)

for Qn in rer.findall(aaa):
    # print Qn
    # if int(Qn)<587122:
    #    continue
    if not Qn.strip():
        continue
    data = wikipedia.DataPage(int(Qn.strip()))
    try:
        items = data.get(throttle=0)
    except:
        continue
    P31 = []
    new_labels = {}
    for claim in items.get('claims', []):
        try:
            if claim['m'][1] == 31:
                 P31.append(claim['m'][3]['numeric-id'])
        except:
            pass
    if not isinstance(items.get('label'),dict) and items.get('label'):
        items['label'] = {}
    if items.get('label', []) == []:
	items['label'] = {}
    for wikilang in items.get('links', []):
        if wikilang in ["commonswiki", "wikidatawiki"]:
            continue
        lang = wikilang.replace("_", "-")
        for i in ["wiki", "voyage", "source", "quote", "news", "books"]:
            lang = wikilang.replace(i, '')
 	if lang == "no":
	    lang = "nb"
	if lang == u"als":
        lang = u"gsw"
    if lang == "simple":
        lang = "en"
    if lang == u"fiu-vro":
        lang = u"vro"
    if lang == u"bat-smg":
        lang = u"sgs"
    if lang == u"be-x-old":
        lang = u"be-tarask"
    if lang == u"roa-rup":
        lang = u"rup"
    if lang == u"zh-classical":
        lang = u"lzh"
    if lang == u"zh-min-nan":
        lang = u"nan"
    if lang == u"zh-yue":
        lang = u"yue"
    if lang == u"crh":
        lang = u"crh-latn"
    if lang == "simple":
        lang = "en"
    error = False
    if wikilang.endswith('wikisource') and items.get('label',{}).get(lang, "").endswith(":"):
        print("probably an error")
        error = True
    if items['label'].has_key(lang) and not error:
        continue
    new_labels[lang] = items['links'][wikilang]['name']
    if 3863 in P31:
        pass
    elif wikilang == "ruwiki":
        if " (" in new_labels[lang] and new_labels[lang].count(" ") == 1 and new_labels[lang].split(" (")[1][0] in capitalls:
            pass
        else:
            new_labels[lang] = new_labels[lang].split(" (")[0]
    elif wikilang == "itwiki" or wikilang == 'frwikisource':
        new_labels[lang] = new_labels[lang].split(" (")[0]
    elif wikilang.endswith('wikisource') and new_labels[lang].startswith(authors2.get(wikilang.replace('wikisource',''),"somtehinndfdgd")+':'):
        new_labels[lang] = new_labels[lang][len(authors2[wikilang.replace('wikisource','')] + ':'):].split(" (")[0]
    else:
        new_labels[lang] = new_labels[lang].split(" (")[0].split(", ")[0]
    if not new_labels:
        continue
    values = {'labels': {}}
    for lang in new_labels:
        values['labels'][lang] = {'language': lang, 'value': new_labels[lang]}
    params = {
        'summary': u"Bot: setting proper label for " + ",".join(new_labels.keys()),
        'format': 'jsonfm',
        'action': 'wbeditentity',
        'id': data.title(),
        'token': site.getToken(),
        'bot': 1,
        'data': json.dumps(values),
        }
    print(data.title())
    try:
        query.GetData(params, site)
    except:
        pass
