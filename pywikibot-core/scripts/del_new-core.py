import pywikibot
import codecs
import re
import json
f=codecs.open("/data/project/dexbot/pywikipedia-git/to_del.txt","r","utf-8")
aaa=f.read()
f.close()
f=codecs.open("/data/project/dexbot/pywikipedia-git/empty1.txt","w","utf-8")
f.write(u"")
f.close()
f=codecs.open("/data/project/dexbot/pywikipedia-git/empty2.txt","w","utf-8")
f.write(u"")
f.close()
rer=re.compile("\[\[Q(\d+?)\]\]")
site=pywikibot.Site('en')
repo = site.data_repository()
famcode = {
    "commons":"commons:",
    "wikisource":"s:",
    "wikiquote":"q:",
    "wikivoyage":"voy:",
    "wikipedia":"",
    "wikinews":"n:",
    'wikibooks':'b:',
}
def _make_old_dict(_contents):
        """Convert the new dictionary to the old one for consistency."""
        if isinstance(_contents.get('claims', {}), list) and not _contents.get('sitelinks'):
            return _contents
        old_dict = _contents
        new_dict = {
            'links': {}, 'claims': [], 'description': old_dict.get('descriptions', []),
            'label': {}}
        for site_name in old_dict.get('sitelinks', []):
            new_dict['links'][site_name] = {
                'name': old_dict['sitelinks'][site_name].get('title'),
                'badges': old_dict['sitelinks'][site_name].get('badges', [])
            }
        for claim_name in old_dict.get('claims', []):
            for claim in old_dict['claims'][claim_name]:
                new_claim = {'m': ['value', int(claim_name[1:]), 'value']}
                new_claim['refs'] = claim.get('references', [])
                new_claim['g'] = claim['id']
                new_claim['q'] = claim.get('qualifiers', [])
                new_claim['m'].append(claim.get('mainsnak', {}).get('datavalue', {}).get('value'))
                new_dict['claims'].append(new_claim)
        for label in old_dict.get('labels',{}):
            new_dict['label'][label] = old_dict['labels'][label]['value']
        return new_dict

def getdeletetimestamp(page):
        site=page.site
        params={"action":"query","list":"logevents","letype":"delete","letitle":page.title()}
        data=pywikibot.data.api.Request(site=site, **params).submit()
        try:
                timestamp=data['query']['logevents'][0]['timestamp']
        except:
                return 0
        return str(timestamp)
havetocheck = True
for number in rer.findall(aaa):
    if not havetocheck:
        continue
    data=pywikibot.ItemPage(repo, "Q" + number)
    try:
        items=data.get()
    except:
        continue
    if items.get('sitelinks'):
        print("links")
        continue
    if items.get('claims'):
        if len(items['claims']) != 1:
            print(items['claims'].keys())
            continue
        if len(items['claims'].get('P31', [])) != 1:
            print(len(items['claims'].get('P31', [])))
            continue
        if items['claims']['P31'][0].target.getID() not in ['Q4167410', 'Q11266439', 'Q13406463']:
            continue
    Do=True
    for linkpage in data.getReferences():
        if linkpage.namespace() not in [2,3]:
            Do=False
        if not Do:
            print("linked")
            continue
        listlinks={}
        ohoh=data.fullVersionHistory()
        for versin in ohoh:
                dd=json.loads(versin.text)
                dd= _make_old_dict(dd)
                if dd.has_key('links'):
                        for iw in dd['links']:
                                if isinstance(dd['links'][iw],dict):
                                        listlinks[iw]=unicode(dd['links'][iw]['name'])
                                else:
                                        listlinks[iw]=unicode(dd['links'][iw])
        if not listlinks:
                print "no list link"
                continue
        ok=[]
        if len(listlinks)!=1:
                print "more than one to check"
                continue
        if "commons" in ",".join(listlinks.keys()) or "voyage" in ",".join(listlinks.keys()) or "quote" in ",".join(listlinks.keys()):
                pass
        if "\"" in u",".join(listlinks.values()):
#                print ",".join(listlinks.values())
                continue
        for wikiname in listlinks:
                langlang=""
                famfam="wikipedia"
                if "source" in wikiname or "voyage" in wikiname or "quote" in wikiname or 'books' in wikiname or 'versity' in wikiname:
                    famfam="wiki"+wikiname.split("wiki")[1]
                elif "commons" in wikiname or "metawiki" in wikiname or "species" in wikiname or "wikidata" in wikiname:
                    famfam=wikiname.split("wiki")[0]
                    langlang= famfam
                if not langlang:
                    langlang=wikiname.split("wiki")[0]
                page=pywikibot.Page(pywikibot.Site(langlang.replace("_","-"), fam=famfam), listlinks[wikiname])
                try:
                        text=page.get()
                except pywikibot.NoPage:
                        timestamp=getdeletetimestamp(page)
                        if timestamp:
                                data.delete("Empty item: the only linked page ([["+famcode.get(famfam,"")+str(page.site)+u":"+page.title()+"]]) was deleted in " + timestamp, False)
                except pywikibot.IsRedirectPage:
                        targetpage=page.getRedirectTarget()
                        try:
                                dataaa=pywikibot.DataPage(page)
                                itemss=dataaa.get()
                        except:
                                ok.append("Double shit")
                        else:
                                nnumber=int(dataaa.title().replace("Q",""))
                                if nnumber<int(number):
                                        data.delete("Empty item: the article ([["+famcode.get(famfam,"")+str(page.site())+u":"+page.title()+"]]) got redirected to ([["+str(targetpage.site())+u":"+targetpage.title()+"]])[[Q"+ str(nnumber)+"]]", False)
                                else:
                                        f=codecs.open("empty2.txt","a","utf-8")
                                        f.write("\n[[Q"+str(nnumber)+"]][[Q"+str(number)+"]]")
                                        f.close()
                except:
                        print "WOW"
                        ok.append("OH shit")
                else:
                        try:
                                dataaa=pywikibot.ItemPage.fromPage(page)
                                itemss = dataaa.get()
                        except:
                                pass
                        else:
                                checkdesc=True
                                if not itemss.get('description'):
                                    itemss['description'] = {}
                                if not items.get('description'):
                                    items['description'] = {}
                                for lang in itemss['description']:
                                    if items['description'].get(lang,"") and items['description'][lang] != itemss['description'][lang]:
                                        checkdesc=False
                                if checkdesc:
                                    #try:
                                    #    data.mergeInto(dataaa)
                                    #except:
                                    #    pass
                                    data.editEntity(data={}, clear=True)
                                    data.set_redirect_target(dataaa, force=True)
                                    #nnumber=int(dataaa.title().replace("Q",""))
                                    #data.delete("Empty item: Merged with [[Q%d]]" %nnumber, False)
