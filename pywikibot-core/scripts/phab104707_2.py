import pywikibot
import codecs
import re
site = pywikibot.Site('en')
repo = site.data_repository()
path = '/data/project/dexbot/pywikipedia-git/res104707.txt'
with codecs.open(path, 'r', 'utf-8') as f:
    cases = re.findall('\[\[(.+?)\]\]', f.read())
ok = False
for title in cases:
    if int(title[1:]) == 2372353:
        ok = True
    if not ok:
        continue
    item = pywikibot.ItemPage(repo, title)
    try:
        item.get()
    except:
        continue
    data = {'labels': {}}
    for lang in item.labels:
        if '&#039;' in item.labels[lang]:
            data['labels'][lang] = item.labels[lang].replace('&#039;', '\'')
    if data['labels']:
        print(title)
        item.editEntity(data, summary=u'[[phab:T104707]]')
