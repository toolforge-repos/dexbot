import pywikibot
import re
from pywikibot import pagegenerators

site = pywikibot.Site('en')
generator = pagegenerators.SearchPageGenerator(
    'insource:/\* *?\[.+ [Oo]fficial website\]/', site=site, namespaces=[0])


ext = '== External links =='
for page in generator:
    try:
        text = page.get()
    except:
        continue
    if ext not in text:
        continue
    before = ext.join(text.split(ext)[:-1])
    after = text.split(ext)[-1]
    new_after = after
    for case in re.findall(r'(\* *?)\[(.+)( [Oo]fficial website\])', after):
        title = ''
        if ' ' in case[1].strip():
            url = case[1].strip().split(' ')[0]
            title = ' '.join(case[1].strip().split(' ')[1:])
        else:
            url = case[1].strip()
        temp = '* {{Official website|' + url.strip()
        if title:
            temp += '|' + title.strip() + ' official website'
        temp += '}}'
        new_after = new_after.replace(case[0] + '[' + case[1] + case[2], temp)
    if new_after != after:
        page.put(before + ext + new_after, 'Bot: Using official website template')

