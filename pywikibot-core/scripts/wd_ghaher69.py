import pywikibot
import urllib.request
import time
import json
_cache = {}
site = pywikibot.Site('wikidata', fam='wikidata')
url = "http://ores.wmflabs.org/scores/wikidatawiki/?models=reverted&revids="
summary = u'Bot: Auto-reverting based on [[WD:ORES]] {0:.3},{1},{2}'
version = '0.2.1'


def report(diff_id, d_score, g_score):
    file_path = '/data/project/dexbot/public_html/tools/'
    file_path += 'good_faith_editors_wd.txt'
    with open(file_path, 'r') as f:
        the_list = f.read()
    if diff_id + ',' in the_list:
        return
    with open(file_path, 'w') as f:
        f.write(
            ','.join([str(diff_id), str(d_score), str(g_score)]) +
            '\n' + the_list)


def recent_changes_gen(site):
    params = {
        'action': 'query',
        'list': 'recentchanges',
        'rcshow': '!bot|!patrolled',
        'rclimit': 100,
        'rctype': 'edit',
        'rctoponly': '1',
        'rcprop': 'ids',
        'rcnamespace': '0',
    }
    res = pywikibot.data.api.Request(site=site, **params).submit()
    for case in res['query']['recentchanges']:
        yield str(case['revid'])


class Ghaher69Bot(pywikibot.Bot):
    """docstring for Ghaher69Bot"""

    def __init__(self, gen, site):
        super(Ghaher69Bot, self).__init__()
        self.gen = gen
        self.site = site

    def run(self):
        self.site.login()
        self.url = url + u'|'.join(list(self.cache_filter()))
        res = json.loads(
            urllib.request.urlopen(self.url).read().decode('utf-8'))
        for revid in res:
            _cache[revid] = res[revid]['reverted']
            if 'probability' not in res[revid]['reverted']:
                continue
            d_score = res[revid]['reverted']['probability']['true']
            if d_score > 0.97:
                if True:
                    try:
                        self.fuck_it(revid, d_score)
                    except pywikibot.data.api.APIError:
                        continue

    def cache_filter(self):
        for rev in self.gen:
            if rev not in _cache:
                yield rev

    def fuck_it(self, revid, probability):
        params = {
            'action': 'query',
            'prop': 'revisions',
            'meta': 'tokens',
            'rvprop': 'user',
            'type': 'rollback',
            'revids': revid,
        }
        res = pywikibot.data.api.Request(site=self.site, **params).submit()
        token = res['query']['tokens']['rollbacktoken']
        page_id = list(res['query']['pages'].keys())[0]
        user = res['query']['pages'][page_id]['revisions'][0]['user']
        params = {
            'action': 'rollback',
            'pageid': page_id,
            'user': user,
            'summary': summary.format(probability, user, version),
            'token': token
        }
        pywikibot.data.api.Request(site=self.site, **params).submit()


bot = Ghaher69Bot(recent_changes_gen(site), site)
while True:
    bot.run()
    time.sleep(360)
