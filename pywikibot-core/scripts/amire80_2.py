# License: MIT
import pywikibot
import codecs
import json
site = pywikibot.Site('ca')
offset = 0
_base_dir = '/data/project/dexbot/pywikibot-core/'
cases = []

with codecs.open('%samire80.txt' % _base_dir, 'r', 'utf-8') as f:
    for line in f:
        if '--' not in line:
            continue
        line = line.replace('\r', '').replace('\n', '').strip().split('--')
        cases.append({'sourceLanguage': line[1], 'targetLanguage': line[2],
                      'sourceTitle': line[3], 'targetTitle': line[0]})

with codecs.open('res.txt', 'w', 'utf-8') as f:
    f.write(json.dumps(cases))
with codecs.open('%sres.txt' % _base_dir, 'r', 'utf-8') as f:
    cases = json.loads(f.read())
issues = []
for case in cases:
    try:
        source_site = pywikibot.Site(case['sourceLanguage'])
        target_site = pywikibot.Site(case['targetLanguage'])
        source_page = pywikibot.Page(source_site, case['sourceTitle'])
        target_page = pywikibot.Page(target_site, case['targetTitle'])
        target_page.exists()
        source_page.exists()
    except:
        case['notes'] = 'Unknown Error'
        issues.append(case)
        with codecs.open('%serrors.txt' % _base_dir, 'w', 'utf-8') as f:
            f.write(json.dumps(cases))
        continue
    if not target_page.exists() or not source_page.exists():
        case['notes'] = 'Source or target does not exist'
        issues.append(case)
        with codecs.open('%serrors.txt' % _base_dir, 'w', 'utf-8') as f:
            f.write(json.dumps(issues))
        continue
    if target_page.namespace() != source_page.namespace():
        print("Namespaces doesn't match")
        continue
    if source_page.isRedirectPage():
        source_page = source_page.getRedirectTarget()
    if target_page.isRedirectPage():
        target_page = target_page.getRedirectTarget()
    source_item = None
    target_item = None
    try:
        source_item = pywikibot.ItemPage.fromPage(source_page)
    except:
        pass
    try:
        target_item = pywikibot.ItemPage.fromPage(target_page)
    except:
        pass
    if not target_item:
        if not source_item:
            case['notes'] = 'None of them are in Wikidata'
            issues.append(case)
            with codecs.open('%serrors.txt' % _base_dir, 'w', 'utf-8') as f:
                f.write(json.dumps(issues))
        elif source_item.exists():
            try:
                if source_item.getSitelink(target_site):
                    print(source_item.getID())
                else:
                    source_item.setSitelink(target_page)
            except pywikibot.exceptions.NoPage:
                try:
                    source_item.setSitelink(target_page)
                except:
                    pass
    elif not source_item and target_item.exists():
        try:
            target_item.getSitelink(source_site)
        except:
            continue
        if target_item.getSitelink(source_site):
            print(target_item.getID())
        else:
            target_item.setSitelink(source_page)
    else:
        if target_item.getID() != source_item.getID():
            case['notes'] = 'Items need merge'
            issues.append(case)
            with codecs.open('%serrors.txt' % _base_dir, 'w', 'utf-8') as f:
                f.write(json.dumps(issues))
