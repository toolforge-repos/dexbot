# -*- coding: utf-8  -*-
import sys
import pywikibot
import json

def main():
    site = pywikibot.Site('fa')
    user = None
    articles = None
    for arg in sys.argv[1:]:
        if arg.startswith('-user:'):
            user = arg.split('-user:')[1]
        elif arg.startswith('-articles:'):
            articles = arg.split('-articles:')[1].split('|')
    if not user or not articles:
        return False
    page = pywikibot.Page(site, 'User talk:' + user)
    try:
        text = page.get()
    except:
        print('Page not accesible')
        return False
    if u'فهرست مقاله‌های پیشنهادی' in text:
        print('Already there')
        return False
    if u'<!-- NO RECOM -->' in text:
        print('User Forbid')
        return False
    text += u'\n== فهرست مقاله‌های پیشنهادی ==\nاینها فهرستی از مقاله‌های پیشنهادی توسط شما هستند که توسط سیستم پیشنهاد دهنده به دست آمده‌اند:\n*[[:en:'
    text += u']]\n*[[:en:'.join(articles)
    text += u']]\nخاکسار شما ~~~~'
    page.put(text, u'ربات: افزودن پیشنهادها')
    print(json.dumps({'done': 'Done'}))
    return True

main()

