from pywikibot import pagegenerators
import pywikibot
from collections import defaultdict
site = pywikibot.Site('fa')
gen = pagegenerators.CategorizedPageGenerator(pywikibot.Category(site, u'رده:پرونده‌های بدون مجوز قابل خواندن برای ماشین'))
people_and_files = defaultdict(list)
for page in gen:
    print(page.title())
    author = page.oldest_revision.user
    people_and_files[author].append(page.title())
print(len(people_and_files))
for person in people_and_files:
    user_talk_page = pywikibot.Page(site, 'User talk:%s' % person)
    try:
        text = user_talk_page.get()
    except:
        continue
    if 'Dexbot]]' in text:
        continue
    one = len(people_and_files[person]) > 1
    text += u'\n== افزودن الگوی مجوز به تصاویرتان ==\nدرود. '
    if not one:
        text += u'یکی از پرونده‌های شما'
    else:
        text += u'تعدادی از پرونده‌های شما'
    text += ' فاقد [[:رده:مجوزهای حق تکثیر|مجوزهای حق‌تکثیر]] هستند که وجود آن برای پرونده‌ها حیاتی است. فهرست آنها:\n'
    for file_name in people_and_files[person]:
        text += '*[[:%s]]\n' % file_name
    text += u'لطفا مجوز مناسب را به تصاویرتان بیفزایید و گرنه امکان حذف این تصاویر هست.'
    text += u' خاکسار شما، ~~~~'
    try:
        user_talk_page.put(text, u'ربات: افزودن پیامی در رابطه با تصاویری فاقد الگوی مجوز مناسب')
    except:
        continue
